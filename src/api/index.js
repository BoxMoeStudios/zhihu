/**
 * Copyright (c) 2014 Meizu bigertech, All rights reserved.
 * http://www.bigertech.com/
 * @author liuxing
 * @date  14-11-11
 * @description
 *
 */
const Post = require('./Post');
const User = require('./User');
const Topic = require('./Topic');
const API = require('../config/api')
const template = require('lodash/template');

const APIURL = (urlToken, mode, detail) => {
	return template(API[mode][detail])({ url_token: urlToken });
}

module.exports = {
	Post,
	User,
	Topic,
	APIURL,
};
