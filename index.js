/**
 * @author bubao
 * @description
 * @date: 2018-2-13 14:13:30
 * @Last Modified by: bubao
 * @Last Modified time: 2018-05-23 13:13:11
 */

const api = require('./src/api');

module.exports = api;
